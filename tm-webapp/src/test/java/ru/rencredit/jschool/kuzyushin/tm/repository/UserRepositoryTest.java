package ru.rencredit.jschool.kuzyushin.tm.repository;

import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.rencredit.jschool.kuzyushin.tm.config.WebMvcConfiguration;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;

import javax.transaction.Transactional;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebMvcConfiguration.class)
public class UserRepositoryTest {

    @Autowired
    private IUserRepository userRepository;

    private static final User admin = new User();

    private static final User test = new User();

    @Autowired
    private static PasswordEncoder passwordEncoder;

    private static final String adminCredentials = "adminTest";

    private static final String testCredentials = "userTest";

    @BeforeClass
    public static void set() {
        admin.setLogin(adminCredentials);
        admin.setEmail("admin@admin.com");
        admin.setPasswordHash("password-admin");
        admin.setFirstName("first-name-admin");
        admin.setLastName("last-name-admin");
        admin.setMiddleName("middle-name-admin");
        admin.setRole(Role.ADMIN);

        test.setLogin(testCredentials);
        test.setEmail("test@test.com");
        test.setPasswordHash("password-test");
        test.setFirstName("first-name-test");
        test.setLastName("last-name-test");
        test.setMiddleName("middle-name-test");
        test.setRole(Role.USER);
    }

    @Before
    @Transactional
    public void init() {
        userRepository.save(admin);
        userRepository.save(test);
    }

    @After
    @Transactional
    public void deleteUser() {
        userRepository.deleteAll();
    }

    @Test
    public void findByLogin() {
        @Nullable final User user = userRepository.findByLogin(admin.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getId(), admin.getId());
    }

    @Test
    @Transactional
    public void deleteByLogin() {
        Assert.assertEquals(2, userRepository.findAll().size());
        userRepository.deleteByLogin(admin.getLogin());
        Assert.assertEquals(1, userRepository.findAll().size());
        Assert.assertNull(userRepository.findById(admin.getId()).orElse(null));
    }
}
