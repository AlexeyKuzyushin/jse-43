package ru.rencredit.jschool.kuzyushin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IProjectService;
import ru.rencredit.jschool.kuzyushin.tm.api.service.ITaskService;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IUserService;
import ru.rencredit.jschool.kuzyushin.tm.config.WebMvcConfiguration;
import ru.rencredit.jschool.kuzyushin.tm.dto.CustomUser;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;
import ru.rencredit.jschool.kuzyushin.tm.repository.IUserRepository;

import java.util.ArrayList;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebMvcConfiguration.class)
public class TaskServiceTest {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @Autowired
    private IUserService userService;

    @Autowired
    private IUserRepository userRepository;

    private static final String adminCredentials = "adminTest";

    private static final String testCredentials = "userTest";

    private CustomUser admin;

    private CustomUser test;

    private Project projectOne;

    private Project projectTwo;

    private Task taskOne;

    private Task taskTwo;

    @Before()
    public void init() {
        admin = new CustomUser(adminCredentials, adminCredentials, new ArrayList<>(),
                userService.create(adminCredentials, adminCredentials, Role.ADMIN).getId());
        test = new CustomUser(testCredentials, testCredentials, new ArrayList<>(),
                userService.create(testCredentials, testCredentials, Role.USER).getId());

        projectOne = projectService.create(admin.getUserId(), "projectOne", "projectOne");
        projectTwo = projectService.create(test.getUserId(), "projectTwo", "projectTwo");

        taskOne = taskService.create(admin.getUserId(), projectOne.getId(), "taskOne", "taskOne");
        taskTwo = taskService.create(test.getUserId(), projectTwo.getId(), "taskTwo", "taskTwo");
    }

    @After
    public void clearData() {
        userRepository.deleteAll();
        projectService.clear();
        taskService.clear();
    }

    @Test
    public void countAllTasksTest() {
        Assert.assertEquals(2, taskService.count().intValue());
    }

    @Test
    public void findByIdTest() {
        @Nullable final Task task = taskService.findTaskById(taskOne.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getId(), taskOne.getId());
    }

    @Test
    public void findByUserIdAndIdTest() {
        @Nullable final Task task = taskService.findById(test.getUserId(), taskTwo.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getId(), taskTwo.getId());
    }

    @Test
    public void findByNameTest() {
        @Nullable final Task task = taskService.findByName(admin.getUserId(), taskOne.getName());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getId(), taskOne.getId());
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(2, taskService.findAll().size());
    }

    @Test
    public void findAllByUserIdTest() {
        Assert.assertEquals(1, taskService.findAllByUserId(admin.getUserId()).size());
        Assert.assertEquals(1, taskService.findAllByUserId(test.getUserId()).size());
    }

    @Test
    public void findAllByProjectIdTest() {
        Assert.assertEquals(1, taskService.findAllByProjectId(projectOne.getId()).size());
        Assert.assertEquals(1, taskService.findAllByProjectId(projectTwo.getId()).size());
    }

    @Test
    public void removeTaskByIdTest() {
        Assert.assertEquals(2, taskService.findAll().size());
        taskService.removeTaskById(taskOne.getId());
        Assert.assertEquals(1, taskService.findAll().size());
        Assert.assertNull(taskService.findTaskById(taskOne.getId()));
    }

    @Test
    public void removeByUserIdAndIdTest() {
        Assert.assertEquals(2, taskService.findAll().size());
        taskService.removeById(admin.getUserId(), taskOne.getId());
        Assert.assertEquals(1, taskService.findAll().size());
        Assert.assertNull(taskService.findTaskById(taskOne.getId()));
    }

    @Test
    public void removeByNameTest() {
        Assert.assertEquals(2, taskService.findAll().size());
        taskService.removeByName(admin.getUserId(), taskOne.getName());
        Assert.assertEquals(1, taskService.findAll().size());
        Assert.assertNull(taskService.findTaskById(taskOne.getId()));
    }

    @Test
    public void clearTest() {
        Assert.assertEquals(2, taskService.findAll().size());
        taskService.clear();
        Assert.assertEquals(0, taskService.findAll().size());
    }

    @Test
    public void removeAllByUserIdTest() {
        Assert.assertEquals(2, taskService.findAll().size());
        taskService.removeAllByUserId(admin.getUserId());
        Assert.assertEquals(1, taskService.findAll().size());
    }

    @Test
    public void removeAllByProjectIdTest() {
        Assert.assertEquals(2, taskService.findAll().size());
        taskService.removeAllByProjectId(projectOne.getId());
        Assert.assertEquals(1, taskService.findAll().size());
    }

    @Test
    public void updateByIdTest() {
        @NotNull final Task newTask = new Task();
        @NotNull final String name = "other-name";
        @NotNull final String description = "other-description";
        newTask.setId(taskOne.getId());
        newTask.setName(name);
        newTask.setDescription(description);
        newTask.setProject(projectOne);
        taskService.updateById(admin.getUserId(), newTask.getId(), newTask.getName(), newTask.getDescription());
        @Nullable final Task task = taskService.findTaskById(taskOne.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getId(), taskOne.getId());
        Assert.assertEquals(task.getName(), name);
        Assert.assertEquals(task.getDescription(), description);
    }
}
