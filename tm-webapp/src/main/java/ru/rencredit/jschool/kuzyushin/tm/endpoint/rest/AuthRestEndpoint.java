package ru.rencredit.jschool.kuzyushin.tm.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IUserService;
import ru.rencredit.jschool.kuzyushin.tm.dto.Fail;
import ru.rencredit.jschool.kuzyushin.tm.dto.Result;
import ru.rencredit.jschool.kuzyushin.tm.dto.UserDTO;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;

import javax.jws.WebParam;

@RestController
@RequestMapping(value="/rest/auth")
public class AuthRestEndpoint {

    @Autowired
    private IUserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @GetMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public Result login(
            @WebParam(name = "username") final @Nullable String username,
            @WebParam(name = "password") final @Nullable String password
    ) {
        try {
            final UsernamePasswordAuthenticationToken token =
                    new UsernamePasswordAuthenticationToken(username, password);
            final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return new Result(authentication.isAuthenticated());
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @GetMapping(value = "/profile", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserDTO profile() {
        @NotNull final SecurityContext securityContext = SecurityContextHolder.getContext();
        final Authentication authentication = securityContext.getAuthentication();
        final String username = authentication.getName();
        return UserDTO.toDTO(userService.findByLogin(username));
    }

    @GetMapping(value = "/logout")
    public void logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
    }
}
