package ru.rencredit.jschool.kuzyushin.tm.config;

import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.WebApplicationInitializer;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@PropertySource("classpath:application.properties")
@ComponentScan("ru.rencredit.jschool.kuzyushin.tm")
@EnableJpaRepositories("ru.rencredit.jschool.kuzyushin.tm.repository")
public class AppConfiguration implements WebApplicationInitializer {

    @Bean
    public DataSource dataSource(
            @Value("${db.driver}") final String dataSourceDriver,
            @Value("${db.url}") final String dataSourceUrl,
            @Value("${db.username}") final String dataSourceUser,
            @Value("${db.password}") final String dataSourcePassword
    ) {
        final DriverManagerDataSource dataSource =  new DriverManagerDataSource();
        dataSource.setDriverClassName(dataSourceDriver);
        dataSource.setUrl(dataSourceUrl);
        dataSource.setUsername(dataSourceUser);
        dataSource.setPassword(dataSourcePassword);
        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            @Value("${hibernate.show_sql}") final String showSql,
            @Value("${hibernate.hbm2ddl.auto}") final String hbm2ddl,
            @Value("${hibernate.dialect}") final String dialect,
            final DataSource dataSource
    ) {
        final LocalContainerEntityManagerFactoryBean factoryBean;
        factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.rencredit.jschool.kuzyushin.tm.entity");
        final Properties properties = new Properties();
        properties.put("hibernate.show_sql", showSql);
        properties.put("hibernate.hbm2ddl.auto", hbm2ddl);
        properties.put("hibernate.dialect", dialect);
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    @Bean
    public PlatformTransactionManager transactionManager(
            final LocalContainerEntityManagerFactoryBean entityManagerFactory
    ) {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

    @Bean
    public SpringBus cxf() {
        return new SpringBus();
    }

//    @Bean
//    public Endpoint projectEndpointRegistry(final ProjectSoapEndpoint projectSoapEndpoint, SpringBus cxf) {
//        final EndpointImpl endpoint = new EndpointImpl(cxf, projectSoapEndpoint);
//        endpoint.publish("/ProjectEndpoint");
//        return endpoint;
//    }
//
//    @Bean
//    public Endpoint taskEndpointRegistry(final TaskSoapEndpoint taskSoapEndpoint, SpringBus cxf) {
//        final EndpointImpl endpoint = new EndpointImpl(cxf, taskSoapEndpoint);
//        endpoint.publish("/TaskEndpoint");
//        return endpoint;
//    }
//
//    @Bean
//    public Endpoint authEndpointRegistry(final AuthSoapEndpoint authSoapEndpoint, SpringBus cxf) {
//        final EndpointImpl endpoint = new EndpointImpl(cxf, authSoapEndpoint);
//        endpoint.publish("/AuthEndpoint");
//        return endpoint;
//    }

    @Override
    public void onStartup(ServletContext servletContext) {
        final CXFServlet cxfServlet = new CXFServlet();
        final ServletRegistration.Dynamic dynamicCXF = servletContext.addServlet("cxfServlet", cxfServlet);
        dynamicCXF.addMapping("/soap/*");
        dynamicCXF.setLoadOnStartup(1);
    }
}
