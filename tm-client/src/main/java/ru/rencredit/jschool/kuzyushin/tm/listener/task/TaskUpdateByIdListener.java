package ru.rencredit.jschool.kuzyushin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import ru.rencredit.jschool.kuzyushin.tm.endpoint.soap.TaskDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.soap.TaskSoapEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.event.ConsoleEvent;
import ru.rencredit.jschool.kuzyushin.tm.listener.AbstractListener;
import ru.rencredit.jschool.kuzyushin.tm.service.SessionService;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

@Component
public final class TaskUpdateByIdListener extends AbstractListener {

    @NotNull
    private final TaskSoapEndpoint taskSoapEndpoint;

    @NotNull
    private final SessionService sessionService;

    @Autowired
    public TaskUpdateByIdListener(
            final @NotNull TaskSoapEndpoint taskSoapEndpoint,
            final @NotNull SessionService sessionService
    ) {
        this.taskSoapEndpoint = taskSoapEndpoint;
        this.sessionService = sessionService;
    }

    @NotNull
    @Override
    public String name() {
        return "task-update-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update task by id";
    }

    @Override
    @EventListener(condition = "@taskUpdateByIdListener.name() == #event.command")
    public void handler(final ConsoleEvent event) {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(id);
        taskDTO.setName(name);
        taskDTO.setDescription(description);
        sessionService.setListCookieRowRequest(taskSoapEndpoint);
        taskSoapEndpoint.updateTaskById(taskDTO);
        System.out.println("[OK]");
    }
}
