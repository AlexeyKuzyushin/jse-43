package ru.rencredit.jschool.kuzyushin.tm.exception;

public abstract class AbstractException extends RuntimeException{

    public AbstractException(Throwable throwable) {
        super(throwable);
    }

    public AbstractException(String message) {
        super(message);
    }

    public AbstractException() {
    }
}
