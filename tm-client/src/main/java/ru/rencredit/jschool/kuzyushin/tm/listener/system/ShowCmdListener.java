package ru.rencredit.jschool.kuzyushin.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.event.ConsoleEvent;
import ru.rencredit.jschool.kuzyushin.tm.listener.AbstractListener;

import java.util.List;

@Component
public final class  ShowCmdListener extends AbstractListener {

//    @NotNull
//    private List<AbstractListener> commandList;
//
//    @Autowired
//    public ShowCmdListener(final @NotNull List<AbstractListener> commandList) {
//        this.commandList = commandList;
//    }

    @NotNull
    @Override
    public String name() {
        return "commands";
    }

    @NotNull
    @Override
    public String arg() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String description() {
        return "Show application commands";
    }

    @Override
    @EventListener(condition = "@showCmdListener.name() == #event.command || (@showArgListener.arg() == #event.command)")
    public void handler(final ConsoleEvent event) {
//        for (@NotNull final AbstractListener command: commandList)
//            if (command.arg() != null)
//                System.out.println(command.name());
    }
}
