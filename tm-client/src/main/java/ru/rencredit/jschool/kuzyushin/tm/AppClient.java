package ru.rencredit.jschool.kuzyushin.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.rencredit.jschool.kuzyushin.tm.boostrap.ClientBootstrap;
import ru.rencredit.jschool.kuzyushin.tm.config.ClientConfiguration;

public class AppClient
{
    public static void main(@Nullable final String[] args) {
        @NotNull final AnnotationConfigApplicationContext clientContext =
                new AnnotationConfigApplicationContext(ClientConfiguration.class);
        @NotNull final ClientBootstrap clientBootstrap = clientContext.getBean(ClientBootstrap.class);
        clientBootstrap.run(args);
    }
}
