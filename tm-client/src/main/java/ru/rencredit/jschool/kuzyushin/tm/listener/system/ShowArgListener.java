package ru.rencredit.jschool.kuzyushin.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.event.ConsoleEvent;
import ru.rencredit.jschool.kuzyushin.tm.listener.AbstractListener;

import java.util.List;

@Component
public final class ShowArgListener extends AbstractListener {

//    @NotNull
//    private List<AbstractListener> commandList;
//
//    @Autowired
//    public ShowArgListener(final @NotNull List<AbstractListener> commandList) {
//        this.commandList = commandList;
//    }

    @NotNull
    @Override
    public String name() {
        return "arguments";
    }

    @NotNull
    @Override
    public String arg() {
        return "-arg";
    }

    @NotNull
    @Override
    public String description() {
        return "Show application arguments";
    }

    @Override
    @EventListener(condition = "@showArgListener.name() == #event.command || (@showArgListener.arg() == #event.command)")
    public void handler(final ConsoleEvent event) {
//        for (@NotNull final AbstractListener command: commandList)
//            if (command.arg() != null)
//                System.out.println(command.arg());
    }
}
