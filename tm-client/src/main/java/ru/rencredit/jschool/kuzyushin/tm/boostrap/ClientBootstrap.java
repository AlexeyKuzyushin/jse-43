package ru.rencredit.jschool.kuzyushin.tm.boostrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.event.ConsoleEvent;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

import java.lang.Exception;

@Component
public class ClientBootstrap {

    @Autowired
    private ConsoleEvent event;

    @Autowired
    private ApplicationEventPublisher publisher;

    public void run(@Nullable final String[] args){
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        String command = "";
        while (true) {
            command = TerminalUtil.nextLine();
            try {
                event.setCommand(command);
                publisher.publishEvent(event);
            } catch (Exception e) {
                logError(e);
            }
        }
    }

    private boolean parseArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @NotNull final String arg = args[0];
        try {
            event.setCommand(arg);
            publisher.publishEvent(event);
        } catch (Exception e) {
            logError(e);
        }
        return true;
    }

    private static void logError(Exception e) {
        System.err.println(e.getMessage());
        System.err.println("[FAILED]");
    }
}

