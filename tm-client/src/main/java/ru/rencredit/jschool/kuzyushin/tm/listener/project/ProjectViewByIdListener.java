package ru.rencredit.jschool.kuzyushin.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import ru.rencredit.jschool.kuzyushin.tm.endpoint.soap.ProjectDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.soap.ProjectSoapEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.event.ConsoleEvent;
import ru.rencredit.jschool.kuzyushin.tm.listener.AbstractListener;
import ru.rencredit.jschool.kuzyushin.tm.service.SessionService;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

@Component
public final class ProjectViewByIdListener extends AbstractListener {

    @NotNull
    private final ProjectSoapEndpoint projectSoapEndpoint;

    @NotNull
    private final SessionService sessionService;

    @Autowired
    public ProjectViewByIdListener(
            final @NotNull ProjectSoapEndpoint projectSoapEndpoint,
            final @NotNull SessionService sessionService
    ) {
        this.projectSoapEndpoint = projectSoapEndpoint;
        this.sessionService = sessionService;
    }

    @NotNull
    @Override
    public String name() {
        return "project-view-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project by id";
    }

    @Override
    @EventListener(condition = "@projectViewByIdListener.name() == #event.command")
    public void handler(final ConsoleEvent event) {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        sessionService.setListCookieRowRequest(projectSoapEndpoint);
        @Nullable final ProjectDTO projectDTO = projectSoapEndpoint.findProjectById(id);
        if (projectDTO == null) return;
        System.out.println("ID: " + projectDTO.getId());
        System.out.println("NAME: " + projectDTO.getName());
        System.out.println("DESCRIPTION: " + projectDTO.getDescription());
        System.out.println("[OK]");
    }
}
